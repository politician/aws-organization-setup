data "aws_caller_identity" "current" {}

locals {
  account_ids = merge(
    zipmap(
      [for value in aws_organizations_account.accounts : value.name],
      [for value in aws_organizations_account.accounts : value.id]
    ),
    {
      Org = data.aws_caller_identity.current.account_id
    }
  )
}

output "organization_account_id" {
  value = data.aws_caller_identity.current.account_id
}

output "role_name" {
  value = local.role_name
}

output "account_ids" {
  value = local.account_ids
}

output "all_users" {
  value = keys(local.users_list)
}

output "user_passwords" {
  sensitive = true
  value = [
    for value in aws_iam_user_login_profile.users : tomap({
      "user"     = value.user,
      "password" = value.encrypted_password
    })
  ]

}

output "terraform_user_keys" {
  sensitive = true
  value = [
    for value in aws_iam_access_key.terraform_users : tomap({
      "user"                  = value.user,
      "aws_access_key_id"     = value.id,
      "aws_secret_access_key" = value.encrypted_secret,
      "account"               = regex("(.*)_terraform$", value.user)[0],
      "role_arn"              = "arn:aws:iam::${local.account_ids[title(regex("(.*)_terraform$", value.user)[0])]}:role/${local.role_name}"
    })
  ]
}
