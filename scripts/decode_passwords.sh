#!/bin/bash

echo ""
echo "*****************************************"
echo "****************  USERS  ****************"
echo "*****************************************"
echo ""
echo "IAM Signin URL: https://$(terraform output organization_account_id).signin.aws.amazon.com/console"
echo ""

for row in $(terraform output --json user_passwords | jq -rc '.[]'); do

  _user() {
    echo ${row} | jq -r '.user'
  }

  _password() {
    echo ${row} | jq -r '.password' | base64 --decode | keybase pgp decrypt
  }

  echo "###### $(_user) ######"
  echo "user    : $(_user)"
  echo "password: $(_password)"
  echo ""
done

echo ""
echo "*****************************************"
echo "***********  TERRAFORM USERS  ***********"
echo "*****************************************"
echo ""

for row in $(terraform output --json terraform_user_keys | jq -rc '.[]'); do

  _user() {
    echo ${row} | jq -r '.user'
  }

  _key() {
    echo ${row} | jq -r '.aws_access_key_id'
  }

  _secret() {
    echo ${row} | jq -r '.aws_secret_access_key' | base64 --decode | keybase pgp decrypt
  }

  _account() {
    echo ${row} | jq -r '.account'
  }

  _role() {
    echo ${row} | jq -r '.role_arn'
  }

  echo "###### $(_account) ######"
  echo "[$(_user)]"
  echo "aws_access_key_id = $(_key)"
  echo "aws_secret_access_key = $(_secret)"
  echo ""

  if [ $(_account) != 'org' ]
  then
  echo "[$(_account)_role]"
  echo "role_arn = $(_role)"
  echo "source_profile = $(_user)"
  echo ""
  fi

  echo ""

done