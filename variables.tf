/**
 * AWS Variables
 */

variable "aws_default_region" {
  type        = string
  description = "Default region to deploy to on AWS"
  default     = "us-east-1"
}

variable "aws_profile" {
  type        = string
  description = "AWS credentials profile to use"
  default     = "default"
}

variable "pgp_key" {
  type        = string
  description = "PGP Key to encrypt/decrypt IAM secrets"
}

variable "accounts" {
  type        = map(string)
  description = "Account names and an email associated to each of them"
}


/**
 * Local Variables
 */

locals {
  role_name   = "OrganizationAccountAccessRole"
  groups_list = concat(["admin"], keys(var.accounts))
}

locals {
  console_users_list   = zipmap(concat(["org_admin"], formatlist("%s_admin", keys(var.accounts))), local.groups_list)
  terraform_users_list = zipmap(concat(["org_terraform"], formatlist("%s_terraform", keys(var.accounts))), local.groups_list)
}

locals {
  users_list = merge(local.console_users_list, local.terraform_users_list)
}

locals {
  common_tags = {
    Terraform  = "true"
    Automation = "true"
  }
}
