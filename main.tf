/*
 * Create Organization
 */

resource "aws_organizations_organization" "org" {
}

/*
 * Create Accounts
 */

resource "aws_organizations_account" "accounts" {
  for_each = var.accounts

  name      = title(each.key)
  email     = each.value
  role_name = local.role_name
  tags = merge(local.common_tags, {
    Environment = each.key
  })
  depends_on = [aws_organizations_organization.org]
}

resource "aws_iam_policy" "account_policies" {
  for_each = var.accounts

  name   = "${title(each.key)}AccountPolicy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "sts:AssumeRole"
      ],
      "Resource": [
        "arn:aws:iam::${aws_organizations_account.accounts[each.key].id}:role/${local.role_name}"
      ]
    }
  ]
}
EOF
}

/*
 * Create Groups
 */

resource "aws_iam_group" "groups" {
  for_each = toset(local.groups_list)

  name = title(each.key)
}

resource "aws_iam_group_policy_attachment" "account_group_policies" {
  for_each = var.accounts

  group      = aws_iam_group.groups[each.key].id
  policy_arn = aws_iam_policy.account_policies[each.key].id
}

// Org Admin Group Policy
resource "aws_iam_group_policy_attachment" "admin_policy" {
  group      = aws_iam_group.groups["admin"].id
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

/*
 * Create Users
 */

resource "aws_iam_user" "users" {
  for_each = local.users_list

  name          = each.key
  force_destroy = true
  tags          = local.common_tags
}

// Attach each user to its group
resource "aws_iam_user_group_membership" "users_groups" {
  for_each = local.users_list

  user   = aws_iam_user.users[each.key].name
  groups = [aws_iam_group.groups[each.value].name]
}

// Give admin users access to all environment groups
resource "aws_iam_user_group_membership" "admin_users_groups" {
  for_each = toset(["org_terraform", "org_admin"])

  user   = aws_iam_user.users[each.key].name
  groups = [for value in aws_iam_group.groups : value.name]
}

// Create Console access for users
resource "aws_iam_user_login_profile" "users" {
  for_each = local.console_users_list

  user                    = aws_iam_user.users[each.key].name
  pgp_key                 = var.pgp_key
  password_reset_required = false
}

// Create Programmatic access for terraform users
resource "aws_iam_access_key" "terraform_users" {
  for_each = local.terraform_users_list

  user    = aws_iam_user.users[each.key].name
  pgp_key = var.pgp_key
}
