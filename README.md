# Setup an AWS organization and its sub-accounts with Terraform

It is now an accepted AWS best practice to create an organization and sub-accounts per environment or project.
However, setting up the groups, roles and permissions always takes time, so this project is aimed at all the lazy out there who want to spin up (and maintain!) in no time their AWS organization structure.

It will create the following structure:

```yaml
- Organization
  - Account 1
  - Account 2
  - ...
```

And the following IAM

```yaml
- Group to manage organization (admin)
  - Terraform admin user
  - Admin user
- Group to manage Account 1
  - Terraform account admin user
  - Account admin user
- ...
```

Whenever you have a new project or environment just add it to your `terraform.tfvars` and apply it to add new accounts to your organization. **That's it!**

## Prerequisite

- An AWS account which is not part of an organization or that has admin rights in an organization
- [Terraform](https://www.terraform.io/downloads.html)
- [Keybase](https://keybase.io/download) account (recommended) or a PGP public key
- [jq](https://stedolan.github.io/jq/)

> If you use homebrew on Mac OSX, you can just run `brew install terraform jq && brew cask install keybase`

## Configure

Ensure your AWS Credentials are set either with [environment variables](https://www.terraform.io/docs/providers/aws/index.html#environment-variables) or the [AWS Cli](https://aws.amazon.com/cli/)

### Variables

[Set the variables](https://www.terraform.io/docs/configuration/variables.html#assigning-values-to-root-module-variables) accordingly.

For example:

```sh
cat << EOF >> terraform.tfvars
aws_default_region = "us-east-1"
pgp_key            = "keybase:romainbarissat"
accounts = {
  dev  = "myproject+dev@gmail.com"
  test = "myproject+test@gmail.com"
  prod = "myproject+prod@gmail.com"
}
EOF
```

> If you are using [AWS CLI](https://aws.amazon.com/cli/) to manage your AWS credentials, you may have different profiles and should set the variable `aws_profile`, otherwise leave it empty and it will default to the `[default]` profile.

### Backend

If you want to use a [remote backend](https://www.terraform.io/docs/backends/types/index.html) such as [Terraform Cloud](https://app.terraform.io/), just create a file `backend.tf`

For example:

```sh
cat << EOF >> backend.tf
terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "my-org"
    workspaces {
      name = "my-workspace"
    }
  }
}
EOF
```

## Usage

1. Initialize terraform and its modules

   ```sh
   terraform init
   ```

2. Test your plan

   ```sh
   terraform plan
   ```

3. Apply your plan (deploy the infrastructure)

   ```sh
   terraform apply
   ```

### Decoding passwords

[Terraform encrypts the password](https://www.terraform.io/docs/providers/aws/r/iam_user_login_profile.html#attributes-reference) by default, so this repo contains a little script to decode them easily.

Assuming you used keybase and a keybase user logged in on your current machine to encode the passwords and access keys, you can run the following script:

```sh
./scripts/decode_passwords.sh
```

You can also execute this code to obtain a single user password (in this case the org_admin user):

```sh
terraform output --json user_passwords | jq -r -c '.[] | select(.user == "org_admin") | .password' | base64 --decode | keybase pgp decrypt
```

Or the following code to obtain a single access key:

```sh
terraform output --json terraform_user_keys | jq -r -c '.[] | select(.user == "org_terraform") | .aws_secret_access_key' | base64 --decode | keybase pgp decrypt
```

### Deleting accounts

AWS won't let you [delete accounts easily](https://docs.aws.amazon.com/organizations/latest/userguide/orgs_manage_accounts_remove.html). Instead you will have to make your sub-accounts as standalone accounts and request these to be closed.

Basically, you will have to:

1. Reset the password of the root user of the account (using the email you supplied for each account)
2. Sign in and select a payment method + support plan
3. Wait a few days (~ 1 week)
4. Delete the account from the organization (just remove it from the accounts variable and apply your plan)
5. Sign in the account and [close it](https://aws.amazon.com/premiumsupport/knowledge-center/close-aws-account/)

## Contributing

If you want to improve this code, please do so and don't hesitate to submit a PR!
